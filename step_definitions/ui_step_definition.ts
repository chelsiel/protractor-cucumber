//stepdef.spec.js
import { Given, When, And, Then } from 'cucumber';
import { browser, ElementFinder, element, by } from 'protractor';
var Request = require("request");
import {expect} from 'chai';

Given('I go to angularjs.org', async function () {
    // Load the AngularJS homepage.
    await browser.get('https://angular.io/');

});

When("I select the Docs tab", async function () {
    //Select the documents tab
    var docButton = element(by.xpath('/html/body/aio-shell/mat-toolbar/mat-toolbar-row[2]/aio-top-menu/ul/li[2]/a'));
    await docButton.click();
});

When("I click on Get Started", async function() {
    var getStartedButton = element(by.xpath('//*[@id="docs"]/aio-doc-viewer/div/div[2]/div/a[1]'));
    await getStartedButton.click();
});

Then("I should see setting up local environment and workspace", async function() {
    await expect(element(by.id('setting-up-the-local-environment-and-workspace')).isDisplayed());
});

Given("I send a request", async function(){
    //URL to hit and content of type of web page
    await Request.get({
        "headers": { "content-type": "application/json" },
        "url": "https://chercher.tech/sample/api/product/read?id=90"})
});

Then("I get a 200 response code", async function(){
    await function (error, response, body){
        // if there's an error print it to the log
        if(error) {
            return browser.logger.info("error: ", error);
        }
        // return text on page
        browser.logger.info("Body :",  JSON.parse(body));

        // assert response code is 200
        browser.logger.info("Response Code :"+response.statusCode);
        expect(response.statusCode).toBe(200);
    }
});
