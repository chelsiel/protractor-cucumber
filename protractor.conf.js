/*
Basic configuration to run your cucumber
feature files and step definitions with protractor.
**/
exports.config = {

    seleniumAddress: 'http://localhost:4444/wd/hub',

    capabilities: {
        browserName:'chrome'
    },

    framework: 'custom',  // set to "custom" instead of cucumbe.

    frameworkPath: require.resolve('protractor-cucumber-framework'),  // path relative to the current config file

    specs: ['./features/*.feature'],     // Specs here are the cucumber feature files


    // cucumber command line options
    cucumberOpts: {

        require: './step_definitions/*.ts',  // require step definition files before executing features
        strict: true,                  // <boolean> fail if there are any undefined or pending steps
        format: "node_modules/cucumber-pretty",            // <string[]> (type[:path]) specify the output format, optionally supply PATH to redirect formatter output (repeatable)
    },

    onPrepare() {
        browser.manage().window().maximize(); // maximize the browser before executing the feature files
        require('ts-node').register({
        project: require('path').join(__dirname, './package.json')})
    },
    resultJsonOutputFile: 'report.json',
    typeRoots: ["node_modules/@types"]

};
