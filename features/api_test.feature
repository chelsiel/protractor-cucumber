Feature: Running Cucumber with Protractor
  As a user of Protractor
  I should be able to use Cucumber
  In order to run my API tests

  Scenario: Send request to website
    Given I send a request
    Then I get a 200 response code
