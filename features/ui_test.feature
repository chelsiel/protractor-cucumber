Feature: Running Cucumber with Protractor
  As a user of Protractor
  I should be able to use Cucumber
  In order to run my UI tests

  Scenario: Go to Angular
    Given I go to angularjs.org
    When I select the Docs tab
    And I click on Get Started
    Then I should see setting up local environment and workspace

